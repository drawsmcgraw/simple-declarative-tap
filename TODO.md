# Working Tasks
- Configure air gapped requirements
    - Admittedly working backwards as we aren't airgapped now

## Nice to have
- (DREW) Add steps for imgpkg'ing packages
- Configure additional hardening

## Nicer to have
- (DREW) Is there anyway to get a LB assigned premature?


## Notes
- Git will all be done via SSH keys