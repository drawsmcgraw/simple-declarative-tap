# TAP Install

## Prereqs
Some are optional, but for the sake of your sanity, let's say they are all required.

1. Install k9s + Kubectl plugins - krew, view-allocations, tree
1. Install kctrl, kp, imgpkg, ytt, kapp (Carvel tools)
2. Install Tanzu CLI, with plugins (secret, etc)
4. Accept the EULA's for TAP
5. Migrate the installation containers per the [docs](https://docs.vmware.com/en/VMware-Tanzu-Application-Platform/1.2/tap/GUID-install.html#relocate-images-to-a-registry-0)
   
## Setup Steps
### Secrets / Config
1. Copy config files and update the values accordingly.
    ```bash
    cp tap-values-clean.yaml tap-values.yaml
    ```

### TAP 
1. Install TAP
    ```bash
    kapp deploy -a tap-install -f <(ytt -f ./tap --data-values-file tap-values.yaml) --wait-check-interval 15s -y
    ```
    This command can take anywhere from 5-15 minutes to fully complete the install of everything.

    Occasionally this command fail to complete successfully. Troubleshoot the error and feel free to run the command again to push the process along to full completion.
    

## Setting up apps / dev stuff
### TAP
1. Setup the developer namespace for TAP (dev-test in this example)
    ```bash
    kubectl create ns dev-test

    # set env values for REGISTRY_SERVER & REGISTRY_USERNAME & REGISTRY_PASSWORD
    export REGISTRY_SERVER="harbor.deepsky.lab"
    export REGISTRY_USERNAME='' 
    export REGISTRY_PASSWORD=""

    kubectl create secret docker-registry registry-credentials --docker-server=${REGISTRY_SERVER} --docker-username=${REGISTRY_USERNAME} --docker-password=${REGISTRY_PASSWORD} -n dev-test

    kubectl apply -f kubernetes/dev-space/setup.yaml -n dev-test

    # This is required for Openshift - in supported versions in the future this will be a lesser permission
    kubectl create clusterrolebinding default-cluster-admin-binding --clusterrole=cluster-admin --serviceaccount=dev-test:default
    ```

2. Sample Workload to test (optional)
    ```
    tanzu apps workload create tanzu-java-web-app -f kubernetes/dev-space/workload.yaml --namespace dev-test

    tanzu apps workload tail tanzu-java-web-app -n dev-test
    ```